> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331

## Ayanna Chukes

### Assignment 3 # Requirements:


1. Field to enter U.S. dollar amount: 1–100,000
2. Must include toast notification if user enters out-of-range values
3. Radio buttons to convert from U.S. to Euros, Pesos and Canadian currency (must be vertically and horizontally aligned)
4. Must include correct sign for euros, pesos, and Canadian dollars
5. Must add background color(s) or theme
6. Create and displaylauncher icon image
7. Create Splash/Loading Screen (Extra Credit: 10 pts.)


#### README.md file should include the following items:

1. Course title, your name, assignment requirements  , as per A1;
2. Screenshot of running application’s unpopulated user interface;
3. Screenshot of running application’s toast notification;
4. Screenshot of runnng application’s converted currency user interface;

Deliverables:
1. Provide Bitbucket read-only access to lis4331repo, include links to the repos you created in the above tutorials in README.md, using Markdown syntax(README.md must also include screenshots as per above.)
2. Blackboard Links: lis4331 Bitbucket

#### Assignment Screenshots:

*Screenshot of unpopulated user interface running*:

![Unpopulated Screenshot](img/currency1.png)

*Screenshot of populated user interface running*:

![Converted Currency Screenshot](img/currency3.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/acc16/bitbucketstationlocations/ "Bitbucket Station Locations")


#### Questions:
1.
Each of the following are a common state for the MediaPlayer class except for which 
one?
a. continue()

2.
If a Java View list exceeds the length of the screen, what is done to the list?
a. the list is made scrollable

3.
If the primary purpose of a class is to display a ListView control, which of the following 
controls is recommended to extend the class from?
b. ListActivity

4.
In a camera app, the next step in the life cyc
le after tapping the button to take a picture 
is to call on which method?
d. onStop() to hide the live image

5.
In what is the
reference for an array enclosed?
b. []

6.
On an Android, what is 1000 milliseconds equivalent to in seconds?
b. 1 second

7.
What can be described as the series of actions from the beginning of an 
Activity to its 
end?
a. life cycle

8.
What does the acronym URL stand for?
a. Uniform Resource Locator

9.
What does the splash scre
en provide time for Android to do?
c. initialize resources for your app

10.
What invokes a scheduled Timer?
d. TimerTask

11.
What is the 
TextView property for backgrounds?
b. background

12.
What is the generic layout that displays checkboxes?
d. simple_list_item_checked

13.
What is the generic layout to a 
simple list?
a. simple_list_item_1

14.
What is the most common file type of media supported for audio playback on the 
Android?
b. mp3

15.
What is used so 
apps can talk to each other in a very simple way?
c. intents

16.
What method marks the end of an Activity?
c. onDestroy
 
17.
What method projects your data to the onscreen list by connecting a 
ListView object to 
array data?
c. setListAdapter method

18.
What should end the block of code in each case statement to prevent the subsequent 
case statement from being execu
ted as well?
b. break;

19.
What statement should you put at the end of a switch statement if you want a block of 
code to be executed in the event none of the case statement values matches?
a. default:

20.
What type of variable is visible in multiple methods throughout the program?
b. class variable

21.
What unit of time is used when scheduling a timer?
a. milliseconds

22.
When a list item is selected, what part of it is passed to the executing code?
c. position

23.
When buying a phone, what specification should be considered to allow more music to 
be stored?
a. memory

24.
When opening a Web site with an intent, what is the last argument?
c. URI

25.
Where is the strings.xml file located?
d. res/values folder

26.
Where should you place an image file that will be used as a background image for a 
TextView control?
c. res\drawable folder

27.
Which ListView control provides a two
-
level list?
d. ExpandableListView

28.
Which XML code statement places an icon named ic_city.png to the right of the text in a 
ListView display?
a. android:drawableRight="@drawable/ic_city"

29.
Which browser is not supported in Android?
b. Internet Explorer

30.
Which feature in Java executes a one
-
time task?
d. timer

31.
Which intent action opens a Web browser on the Android?
a. ACTION_VIEW

32.
Which is the correct statement to make a button named btnStart visible?
d. btnStart.setVisibility(View.VISIBLE);

33.
Which method is called when an item in a list is selected?
d. onListItemClick

34.
Which method will 
temporarily stop music while it is playing?
b. pause

35.
Which of the following actions can the MediaPlayer class NOT do?
a. play different music formats

36.
Which 
of the following classes comes with the Android platform to play audio and video 
files?
b. MediaPlayer

37.
Which of the following controls allows you to create a two
-
level list?
a. ExpandableListView control

38.
Which of the following is the correct syntax for the beginning of a switch statement that 
evaluates a variable named choice?
d. switch(choice) {

39.
Which of 
the following methods hides an Activity?
d. onStop

40.
Which of the following statements creates a new instance of MediaPlayer and assigns it 
to mpTrombone?
d. mpTrombone = new MediaPlayer();

41.
Which of the following terms provides a data model for the layout of the list?
b. adapter

42.
Which property determines whether a control is 
displayed or not?
d. visibility property

43.
Which property of a control determines whether the control is displayed on the screen?
b. Visibility

44.
Which property of an Acti
vity determines whether the Activity is active, paused, stopped, 
or dead.
c. state

45.
Which statement allows you to evaluate a value and execute a block of statements 
based on an integer or single character input?
d. switch statement

46.
Which statement in a custom XML layout for a ListView sets the size of the test to 20sp?
a. android:textSize="20sp"

47.
Which
type of control do you use to contain a list of items that allows a user to select an 
item in the list for further action?
c. ListView

48.
Which type of variable ceases to exist when execution of its declaring method
completes?
c. local

49.
Which variable allows you to store multiple values in one variable at the same time?
a. array variable

50.
Why might a music player fail to play a g
iven file?
a. the file format is unsupported






