package com.example.currencyconverter;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;


public class MainActivity extends AppCompatActivity {
    double radtoEuro = 0.88;
    double amountEntered = 0.00;
    double convertedAmount = 0.00;
    double radtoPeso = 19.27;
    double radtoCanada = 1.32;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
            final EditText amount = findViewById(R.id.txtAmount);
            final RadioButton euro = findViewById(R.id.radEuros);
            final RadioButton peso = findViewById(R.id.radPesos);
            final RadioButton canada = findViewById(R.id.radCanada);
            final TextView result = findViewById(R.id.txtResult);
        Button convert = findViewById(R.id.Convert);
        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amountEntered = Double.parseDouble(amount.getText().toString());
                DecimalFormat hundredth = new DecimalFormat("###,###.##");
                if(euro.isChecked()) {
                    convertedAmount = amountEntered * radtoEuro;
                    result.setText(hundredth.format(convertedAmount) + "euros");
                }else if(peso.isChecked()) {
                    convertedAmount = amountEntered * radtoPeso;
                    result.setText(hundredth.format(convertedAmount) + "pesos");
                } else if(canada.isChecked()) {
                    convertedAmount = amountEntered * radtoCanada;
                    result.setText(hundredth.format(convertedAmount) + "canadian");
                } else {
                    Toast.makeText(MainActivity.this, "Invalid entry!", ( Toast.LENGTH_LONG )).show();
                }
            }
        });
    }
}