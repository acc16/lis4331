> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331

## Ayanna Chukes

### Assignment 5 # Requirements:


1. Include splash screen image, app title, intro text.
2. Must use your own RSS feed
3. Must add background color(s) or theme
4. Create and displaylauncher icon image


#### README.md file should include the following items:

1. Course title, your name, assignment requirements  , as per A1;
2. Screenshot of running application’s splash screen;
3. Screenshot of running application’s invalid screen;
4. Screenshot of runnng application’s valid screen;

Deliverables:
1. Provide Bitbucket read-only access to lis4331 repo, include links to the repos you created in the above tutorials in README.md, using Markdown syntax(README.md must also include screenshots as per above.)
2. Blackboard Links: lis4331 Bitbucket

#### Assignment Screenshots:

*Screenshot of unpopulated user interface running*:

![Splash Screen Screenshot](img/)

*Screenshot of populated user interface running*:

![Individual Article Screenshot](img/)

*Screenshot of populated user interface running*:

![Default Browser Screenshot](img/)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A4 Bitbucket Link](https://bitbucket.org/acc16/bitbucketstationlocations/ "Bitbucket Station Locations")


