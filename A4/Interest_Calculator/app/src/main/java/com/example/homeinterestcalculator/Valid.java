package com.example.homeinterestcalculator;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;

public class Valid extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_valid);

        TextView paid = (TextView) findViewById(R.id.txtPaid);
        ImageView image3 = (ImageView) findViewById(R.id.img3);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        float intMonthly = sharedPref.getFloat("key1", 0);
        int intYears = sharedPref.getInt("key2", 0);
        float decPrincipal = sharedPref.getFloat("key3", 0);
        float decPaidAmount;
        decPaidAmount = ( intMonthly * ( 12 * intYears ) - decPrincipal);
        DecimalFormat currency = new DecimalFormat("$###,###.##");
        paid.setText("Monthly Payment: " + currency.format(decPaidAmount));
        if (intYears == 10) {
            image3.setImageResource(R.drawable.image1);
        } else if (intYears == 20) {
            image3.setImageResource(R.drawable.image2);
        } else if (intYears == 30) {
            image3.setImageResource(R.drawable.image3);
        } else {
            paid.setText("Enter 10, 20, or 30 years");
        }


    }
}


