package com.example.homeinterestcalculator;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
            float intMonthly;
            int intYears;
            float decPrincipal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.icon);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        final TextView monthly = (TextView) findViewById(R.id.txtMonthly);
        final TextView years = (TextView) findViewById(R.id.txtYears);
        final TextView principal = (TextView) findViewById(R.id.txtPrincipal);
        Button button= (Button) findViewById(R.id.btnPayment);
        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             intMonthly = Float.parseFloat(monthly.getText().toString());
             intYears = Integer.parseInt(years.getText().toString());
             decPrincipal = Float.parseFloat(principal.getText().toString());
             SharedPreferences.Editor editor = sharedPref.edit();
             editor.putFloat("key1", intMonthly);
             editor.putInt("key2", intYears);
             editor.putFloat("key3", decPrincipal);
             editor.commit();
             startActivity(new Intent(MainActivity.this, Valid.class));

            }
        });

    }
}
