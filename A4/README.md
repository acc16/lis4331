> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331

## Ayanna Chukes

### Assignment 4 # Requirements:


1. Include splash screen image, app title, intro text.
2. Include appropriate images
3. Must use persistent data: SharedPreferences
4. Widgets and images must be vertically and horizontally aligned
5. Must add background color(s) or theme
6. Create and displaylauncher icon image


#### README.md file should include the following items:

1. Course title, your name, assignment requirements  , as per A1;
2. Screenshot of running application’s splash screen;
3. Screenshot of running application’s invalid screen;
4. Screenshot of runnng application’s valid screen;

Deliverables:
1. Provide Bitbucket read-only access to lis4331 repo, include links to the repos you created in the above tutorials in README.md, using Markdown syntax(README.md must also include screenshots as per above.)
2. Blackboard Links: lis4331 Bitbucket

#### Assignment Screenshots:

*Screenshot of unpopulated user interface running*:

![Splash Screen Screenshot](img/)

*Screenshot of populated user interface running*:

![Invalid Screenshot](img/)

*Screenshot of populated user interface running*:

![Valid Screenshot](img/)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A4 Bitbucket Link](https://bitbucket.org/acc16/bitbucketstationlocations/ "Bitbucket Station Locations")


