package com.example.concerttickets;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;



public class MainActivity extends AppCompatActivity     {


   double billTotal = 0.00;
   double numberOfGuests = 0.00;
   double tipNum =0.00;
    double totalCost = 0.00;
   double billAmount = 0.00;
   double tipAmount = 0.00;
   String txtNum ="";
   String txtPercent = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_main);

        final EditText bill = (EditText) findViewById(R.id.editText4);
        final Spinner guests = (Spinner) findViewById(R.id.lstNum);
        final Spinner tip = (Spinner)findViewById(R.id.tipPercentage);

        Button calc = (Button)findViewById(R.id.button);

        calc.setOnClickListener( new View.OnClickListener() {
            final TextView result = (TextView)findViewById(R.id.result);
            @Override
            public void onClick(View v) {
                txtNum = guests.getSelectedItem().toString();
                if(txtNum.equals("1")) {
                    numberOfGuests=1.0;
                }else if (txtNum.equals("2")){
                    numberOfGuests=2.0;
                }else if (txtNum.equals("3")){
                    numberOfGuests=3.0;
                }else if (txtNum.equals("4")){
                    numberOfGuests=4.0;
                }else if (txtNum.equals("5")){
                    numberOfGuests=5.0;
                }else if (txtNum.equals("6")){
                    numberOfGuests=6.0;
                }else if (txtNum.equals("7")){
                    numberOfGuests=7.0;
                }else if (txtNum.equals("8")){
                    numberOfGuests=8.0;
                }else if (txtNum.equals("9")){
                    numberOfGuests=9.0;
                }else if (txtNum.equals("10")){
                    numberOfGuests=10.0;
                }
                txtPercent = tip.getSelectedItem().toString();
                if(txtPercent.equals("0%")){
                    tipAmount=0;
                }else if (txtPercent.equals("5%")){
                    tipAmount=0.05;
                }else if (txtPercent.equals("15%")){
                    tipAmount=0.15;
                }else if (txtPercent.equals("20%")){
                    tipAmount=0.20;
                }else if (txtPercent.equals("25%")){
                    tipAmount=0.25;
                }
                DecimalFormat currency = new DecimalFormat("$###,###.##");
                billTotal = Double.parseDouble(bill.getText().toString());


                tipNum = (billTotal * tipAmount);

                billAmount = tipNum / numberOfGuests;
                totalCost = ((billTotal / numberOfGuests) + billAmount);
                NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);

                    result.setText("Cost for " + guests.getSelectedItem().toString() + " guests is: " + nf.format(totalCost));
                }




            });
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        }




    }



