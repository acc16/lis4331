> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331

## Ayanna Chukes

### Assignment 2 # Requirements:

*Sub-Heading:*

1. Drop-down menu for total number of guests
2. Drop-down menu for tip percentage 
3. Must add background color(s) or theme 
4. Must create and display launcher icon image

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of running application's unpopulated interface;
* Screenshot of running application's populated user interface;


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.

#### Assignment Screenshots:

*Screenshot of unpopulated user interface running*:

![Unpopulated Screenshot](img/.png)

*Screenshot of populated user interface running*:

![Populated Screenshot](img/.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/acc16/bitbucketstationlocations/ "Bitbucket Station Locations")


#### Questions:

1.
What keystroke can you use to invoke possible code completion suggestions as you type 
an XML or Java line of code?
a. Ctrl+space

2.
To 
assign a phrase as the value of a String variable, in which of the following characters 
should the text be placed?

b. double quotation marks

3.
To display text in a TextView control, which method should be
used?

c. setText()

4.
What Java class should you use if you want to format a number such as 279.55 to 
display in currency format such as $279.55?
a. DecimalFormat

5.
What can be used to display instructions at the top of Spinner controls?

d. prompt

6.
What defines a string resource of (multiple) related items in a central location within 
strings.xml?

c. 
string array

7.
What do you use if you want an app to wait for a user to click or tap a button before 
taking action?
a. onClickListener()

8.
What does the R in findViewById(R.id.txtTickets
) refer to?

b. the resources available to the app

9.
What is the on
-
screen keyboard called?

c. soft keyboard

10.
What is used in a Java program to contain data that changes during the execution of the 
program?

d. variable

11.
What method returns the text label from a selected Spinner item?

d.get 
selectedItem()
12.
What must you do before you can use a variable in a Java application?
a. delcare it

13.
What term can be described as multitouch interactions such as pressing two fingers to 
pan,
rotate, or zoom?

b. gestures

14.
Which class should you use if you want to convert the string "123" to the number one 
hundred and twenty
-
three?
a. parseInt()

15.
Which data type is NOT a p
rimitive that holds numeric values?

b. char

16.
Which file is a default file that contains commonly used strings for an application?

c. strings.xml

17.
Which kind of TextView
can suggest the completion of a word after the first few letters?
a. AutoComplete TextView

18.
Which kind of variable, once it has been assigned, always contains the same value?
a. final variable


19.
Which of the following operators has the highest order of precedence?
a. ()

20.
Which of the following terms best describes a widget similar to a drop
-
down list for 
selecting a 
single item from a fixed listing?

c. Spinner control

21.
Which primitive data type should you use if the variable could have only one of two 
values and will be used in conditional statements?

c. boolean

22.
Which term is best defined as a short description of a field that is visible as light
-
colored 
text?

c. hint

23.
You have a variable that will only assume values between 0 and 100. What would be the 
mo
st efficient primitive type to use?
a. byte

24.
Decision
-
making statements in Java allow you to test which of the following?
a. conditions

25.
In what are RadioButton
controls usually used together, which causes only one 
RadioButton to be selected at a time?

c. RadioGroup

26.
In what graphics format should launcher icons be saved?
a. .png

27.
The icon on the h
ome screen that represents your application is referred to as which of 
the following?

c. launcher icon

28.
To compare the alphabetical order of two strings, what should be used?

c. compareTo me
thod

29.
What does a toast notification use to display a message?

b. only the amount of space needed
 
30.
What is a common mistake made when forming a conditional statement?
a. using the 
assignment operator instead of the comparison operator

31.
What is the icon used to start your app called?

b. launcher icon

32.
What is the method used to test a checked property on a radio button?

b. isChecked() method

33.
What is the resulting value of the expression “sears”.compareTo(“chicago”)?

b. greater than 0

34.
What is used to compare Strings in Java for equality?

d. 
equals method

35.
What method call is used to display a toast notification?

b. Toast.makeText

36.
What tool does Java provide to compare the relationship between the variables being 
tested in conditional st
atements?

c. relational operators

37.
When an If statement is placed inside another If statement, what is the inner If 
statement considered to be?

c. nested

38.
When does a 
toast notification disappear from the screen?

b. It automatically fades after a set amount of time.

39.
When more than one condition is included in an If stateme
nt, what is the condition 
called?
a. compound condition

40.
When you have one set of instructions to execute when a condition is true and another 
set of instructions if the same condition is false, w
hich statement would you use?

b. If Else statement

41.
Which of the following is the correct syntax for displaying a toast message for a long 
length of time?

b. Toast.makeText(MainActivity.this, “A toast message”, Toast.LENGTH_LONG).show( );

42.
Which of the following terms best describes a triplet of three colors which encodes 
different colors?

b. hexadecimal color code

43.
Which of the following types 
of controls always operates independently, therefore 
selecting it will never deselect any other controls?
a. CheckBox control


44.
Which of the following types of graphics are best to use for 
icon design because the 
images can be scaled without loss of detail?

d. Vector
-
based graphics

45.
Which property can center a control horizontally as well as position it at other places on 
the scree
n?

b. gravity property

46.
Which symbol is the operator for “equal to?"

b. ==

47.
Which term can be used to change the spacing around each object in your layout?

b. 
margins

48.
Which term defines a fundamental control structure used in computer programming?

c. decision structure

49.
Which statement establishes a currency decimal format so t
hat the variable itemCost is 
displayed in U.S. dollar format in the following statement?
USdollars.format(itemCost));

c. DecimalFormat USdol
lars = new DecimalFormat("$###,###.##");

50.
Choose the option that completes the following code so that the text is displayed in a 
TextView object named displayText.
String sports = "Football";
String fall 
= sports + " season begins in the Fall.";
----
code goes here 
----

d. displayText.setText(fall);
