 

import java.awt.*;
import java.awt.event.*;
import javax.swing.*; 
import java.util.*;

public class BmiGui3 implements ActionListener {
      // BmiGui3 is its own runnable client program
      public static void main(String[] args) {
          BmiGui3 gui = new BmiGui3();
      }

      // onscreen components stored as fields
      private JFrame frame;
      private JTextField heightField;
      private JTextField weightField;
      private JLabel bmiLabel;
      private JButton computeButton;    

      public BmiGui3() {

          // set up components
          heightField = new JTextField(5);
          weightField = new JTextField(5);
          bmiLabel = new JLabel("Compute Distance Leg C");
          computeButton = new JButton("Compute");

         // attach GUI as event listener to Comput
        computeButton.addActionListener(this);

          // layout
          JPanel north = new JPanel(new GridLayout(2, 2));
          north.add(new JLabel("Leg A: "));
          north.add(heightField);
          north.add(new JLabel("Leg B: "));
          north.add(weightField);

          // frame
        frame = new JFrame("Leg C:");
          frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
          frame.setLayout(new BorderLayout());
          frame.add(north, BorderLayout.NORTH);
          frame.add(bmiLabel, BorderLayout.CENTER);
          frame.add(computeButton, BorderLayout.SOUTH);
          frame.pack();
          frame.setVisible(true);
      }

      
      public void actionPerformed(ActionEvent event) {

         // read LegA/LegB info from text fields
         String heightText = heightField.getText();
         double height = Double.parseDouble(heightText);
         String weightText = weightField.getText();
         double weight = Double.parseDouble(weightText);

        // compute LegC and display it onscreen
         double bmi = (weight*weight) + (height * height);
         double bmi2 = Math.sqrt(bmi);
         bmiLabel.setText(String.format("Leg C: %.2f", bmi2));

}
}