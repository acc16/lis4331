import java.util.ArrayList; 
import java.util.Arrays; 
import java.util.Collections;
import java.util.List; 

public class ArrayDemo {
    public static void sum(int[] arr)  { 
        
        int sum = 0; 
          
        for (int i = 0; i < arr.length; i++) 
            
            sum+=arr[i]; 
            
            System.out.println("Sum of all numbers is " + sum); 
    }
    public static void main(String[] args) {
        
        System.out.println("Program creates an array with the following number,");
        System.out.println("and performs the operations bellow using user-defined methods.\n");
        
        int[] arr = {12, 15, 34, 67, 4, 9, 10, 7, 13, 50};
         
        System.out.print("\nNumbers in the array are " + java.util.Arrays.toString(arr).replace("[", "").replace("]", ""));
       
        int[] revArr = {50, 13, 7, 20, 9, 4, 67, 34, 15, 12};

        System.out.print("\nNumbers in reverse order are " + java.util.Arrays.toString(revArr).replace("[", "").replace("]", "") + "\n");
    
        sum(arr);

        avg(arr);

    }
    
    public static void avg(int[] arr)  { 

        double total = 0;

        for(int i=0; i<arr.length; i++)
            
            total = total + arr[i];
    
        double average = total / arr.length;
   
        System.out.format("Average is %.1f\n", average);
        
        for(int i =0; i < arr.length; i++)
            
            if(arr[i] > average) {
                
                System.out.print(arr[i] + " ");

            }     
            System.out.print("are greater than the average");
        }
} 