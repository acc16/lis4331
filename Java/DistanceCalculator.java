import java.awt.util;      
import java.awt.event.util; 
import javax.swing.util;    
 
// A Swing GUI application inherits the top-level container javax.swing.JFrame
public class DistanceCalculator extends JFrame {
   private legA legB, legC;
   private int sum = 0;    // accumulated sum, init to 0
 
   // Constructor to setup the GUI components and event handlers
   public DistanceCalculator() {
      // Retrieve the content-pane of the top-level container JFrame
      // All operations done on the content-pane
      Container cp = getContentPane();
      cp.setLayout(new GridLayout(2, 2, 5, 5));  // The content-pane sets its layout
 
      cp.add(new JLabel("Enter Leg A: "));
      legB = new legA(10);
      cp.add(legB);
      cp.add(new JLabel("The Accumulated Sum is: "));
      legC = new legA(10);
      legC.setEditable(false);  // read-only
      cp.add(legC);
 
      // Allocate an anonymous instance of an anonymous inner class that
      //  implements ActionListener as ActionEvent listener
      legB.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent evt) {
            // Get the String entered into the input TextField, convert to int
            int numberIn = Integer.parseInt(legB.getText());
            sum += numberIn;      // accumulate numbers entered into sum
            legB.setText("");  // clear input TextField
            legC.setText(sum + ""); // display sum on the output TextField
         }
      });
 
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  // Exit program if close-window button clicked
      setTitle("Distance Calculator"); // "super" Frame sets title
      setSize(350, 120);  // "super" Frame sets initial size
      setVisible(true);   // "super" Frame shows
   }
 
   // The entry main() method
   public static void main(String[] args) {
      // Run the GUI construction in the Event-Dispatching thread for thread-safety
      SwingUtilities.invokeLater(new Runnable() {
         @Override
         public void run() {
            new DistanceCalculator(); // Let the constructor do the job
         }
      }); 
   }
}            