import java.io.*;
import java.util.Scanner;

public class FileWrite {

   public static void main(String args[])throws IOException {
      File file = new File("filewrite.txt");
      
      // creates the file
      file.createNewFile();
      
      // creates a FileWriter Object
      FileWriter writer = new FileWriter(file); 
	  Scanner input = new Scanner(System.in);
      System.out.print("Enter text: ");
	  String text = "";
	  
	  text = input.nextLine();
	  
      // Writes the content to the file
	  try {
      //writer.write("Four score and seven years ago our fathers brought forth,\n on this continent, a new nation, concieved in liberty,\n and dedicated to the proposition that all men are created equal\n"); 
      writer.write(text);
	  writer.flush();
      writer.close();
	  }
	  catch (IOException e) {
	  System.err.print("Error: Found IOException- " + file );
	  } finally {
		  writer.close();
	  }
	  

      // Creates a FileReader Object
      FileReader fr = new FileReader(file); 
      char [] a = new char[50];
      fr.read(a);   // reads the content to the array
      
      for(char c : a)
         System.out.print(c);   // prints the characters one by one
      fr.close();
	  
   }
}