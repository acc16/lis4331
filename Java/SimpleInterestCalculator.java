import java.util.Scanner;
import java.lang.String;
import java.text.DecimalFormat;

public class SimpleInterestCalculator {

   public static void main(String args[]) {
   
   
   boolean continueInput = true;
   
   while (continueInput) {
   
   double principal = 0;
   double interestRate = 0;
   double ratePercent = 0;
   int time = 0;
   double total;
   boolean invalidInput;
   String yesNo;
   
   Scanner input = new Scanner(System.in);
   invalidInput = true;
  
   
   while (invalidInput) {
   System.out.print("\nCurrent Principal: $");
   
   if (!input.hasNextDouble()) {
   
   input.next();
   System.out.println("Not a valid number! Try again.");
   }
   else {
   principal = input.nextDouble();
   invalidInput = false;
   }
   }
   invalidInput = true;
      while (invalidInput) {
   System.out.print("\nInterest Rate Per Year: ");
   
   if (!input.hasNextDouble()) {
   
   input.next();
   System.out.println("Not a valid number! Try again.");
   }
   else {
   interestRate = input.nextDouble();
   invalidInput = false;
   }
   }
   invalidInput = true;
      while (invalidInput) {
   System.out.print("\nTotal time in years: ");
   
   if (!input.hasNextInt()) {
   
   input.next();
   System.out.println("Not a valid integer! Try again.");
   }
   else {
   time = input.nextInt();
   invalidInput = false;
   }
   }
   
   ratePercent = interestRate/100;
   total = principal * (1 + (ratePercent * time));
   DecimalFormat money = new DecimalFormat("$0,000.00");
   
   System.out.println("You will have saved " + money.format(total) + " in " + time + " years, at an interest rate of " + interestRate + "%");
   System.out.println("Continue? Y/N: ");
   yesNo = input.next();
   
   if (yesNo.equalsIgnoreCase("n")) {

continueInput = false;

}
}
}
}   