import java.util.Scanner;

public class Book extends Product {

	String author;
	
	public Book () {
		super();
		this.author = "John Doe";
		}
		
		public Book (String c, String d, double p, String author){
		 super(c, d, p);
		 this.author = author;
		 }
		 public String getAuthor() {
		 return author;
		 }
		 public void setAuthor(String a) {
		 author = a;
		 }
		 public void print() {
		 super.print();
		 System.out.println("Author: " + this.getAuthor());
		 }
	}