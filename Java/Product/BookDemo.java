import java.util.Scanner;

class BookDemo {

    public static void main(String[] args){

        String c = "";
        String d = "";
        double p = 0;
		String author;
        Scanner input = new Scanner(System.in);

        System.out.println("/////Below are the default constructor values://///");
        
        Book b1 = new Book();
        System.out.println("\nCode = " + b1.getCode());
        System.out.println("\nDescription = " + b1.getDescription()); 
        System.out.println("\nPrice = $" + b1.getPrice());
		System.out.println("\nAuthor = " + b1.getAuthor());
        
        System.out.println("\n/////Below are user-entered valuses://///");

        System.out.println("\nCode: ");
        c = input.nextLine();

        System.out.println("Description: ");
        d = input.nextLine();

        System.out.println("Price: ");
        p = input.nextDouble();
		 input.nextLine();
		
		System.out.println("Author: ");
		author = input.nextLine();
		

        Book b2 = new Book (c, d, p, author);
		System.out.println("\n/////Below using setter methods to pass literal values, then print() method to display values://///");
        b2.print();
		
		b2.setCode("xyz789");
        b2.setDescription("Test Widget");
        b2.setPrice(89.99);
	
		
		System.out.println("\n/////Below using setter methods to pass literal values, then print() to display values://///\n");
        b2.print();
    }
}