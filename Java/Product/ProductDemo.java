import java.util.Scanner;

class ProductDemo {

    public static void main(String[] args){

        String c = "";
        String d = "";
        double p = 0;
        Scanner input = new Scanner(System.in);

        System.out.println("/////Below are the default constructor values://///");
        
        Product v1 = new Product();
        System.out.println("\nCode = " + v1.getCode());
        System.out.println("\nDescription = " + v1.getDescription()); 
        System.out.println("\nPrice = $" + v1.getPrice());
        
        System.out.println("\n/////Below are user-entered valuses://///");

        System.out.println("\nCode: ");
        c = input.nextLine();

        System.out.println("Description: ");
        d = input.nextLine();

        System.out.println("Price: ");
        p = input.nextDouble();
		p = input.nextLine;

        Product v2 = new Product(c, d, p);
        System.out.print("\nCode = " + v2.getCode());
        System.out.print("Description: " + v2.getDescription());
        System.out.print("Price: $" + v2.getPrice());

        System.out.println("\n/////Below using setter methods to pass literal values, then print() method to display values://///");
        v2.setCode("xyz789");
        v2.setDescription("Test Widget");
        v2.setPrice(89.99);
        v2.print();
    }
}