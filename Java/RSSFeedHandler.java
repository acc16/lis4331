package .....

import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.*;

public class RSSFeedHandler extends DefaultHandler
{
    private RSSFeed feed;
    private RSSItem item;
    private boolean feedTitleHasBeenRead = false;
    private boolean feedPubDateHasBeenRead = false;
    private boolean isTitle = false;
    private boolean isDescription = false;
    private boolean isLink = false;
    private boolean isPubDate = false;
    private boolean isOrigLink = false;
    public RSSFeed getFeed() {
        return feed;
    }
    @Override
    public void startDocument() throws SAXException {
        feed = new RSSFeed();
        item = new RSSItem();
    }

    @Override
    public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
        if (qName.equals("item")) {
            item = new RSSItem();
            return;
        }
        else if (qName.equals("title")) {
            isTitle = true;
            return;
        }
         else if (qName.equals("description")) {
            isDescription = true;
            return;
        }
         else if (qName.equals("link")) {
            isLink = true;
            return;
        }
         else if (qName.equals("pubDate")) {
            isPubDate = true;
            return;
        }
         else if (qName.equals("feedburner:origLink")) {
            isOrigLink = true;
            return;
        }
    }
    @Override
    public void
}