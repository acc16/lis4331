import java.util.Scanner;

public class MeasurementConversion
{
	public static void main (String[] args)
	{
	
	Scanner input = new Scanner (System.in);
	int inches;
	final double INCHES_TO_CENTIMETER = 2.54 ;
	final double INCHES_TO_METER = 0.0254;
	final double INCHES_TO_FOOT =12.0;
	final double INCHES_TO_YARD = 36.0;
	final double FEET_TO_MILE = 5280.0;
	
	

System.out.println("Program converts inches to centimeters, meters, feet,yards, and miles.");
 
System.out.print("Please enter number of inches: ");

while (!input.hasNextInt()){
    System.out.println("Not valid integer \n");
	input.next();
	}
	inches = input.nextInt();
	System.out.println( inches +" inches equals \n");
	
	double centimeter = inches * INCHES_TO_CENTIMETER;
	double meter = inches * INCHES_TO_METER;
	double foot = inches / INCHES_TO_FOOT;
	double yard =inches / INCHES_TO_YARD;
	double mile = foot / FEET_TO_MILE;


System.out.printf(" %.6f centimeters \n", centimeter);
System.out.printf(" %.6f meters \n", meter);
System.out.printf(" %.6f feet \n" , foot);
System.out.printf(" %.6f yards \n", yard);
System.out.printf(" %.6f miles \n", mile);

	}
}

 
