> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331

## Ayanna Chukes

### Project 2 # Requirements:


1. Include splash screen.
2. Insert at least 5 sample tasks
3. Must add background color(s) or theme
4. Create and displaylauncher icon image


#### README.md file should include the following items:

1. Course title, your name, assignment requirements  , as per A1;
2. Screenshot of running application’s task list;


Deliverables:
1. Provide Bitbucket read-only access to lis4331 repo, include links to the repos you created in the above tutorials in README.md, using Markdown syntax(README.md must also include screenshots as per above.)
2. Blackboard Links: lis4331 Bitbucket

#### Assignment Screenshots:

*Screenshot of unpopulated user interface running*:

![Task List Screenshot](img/)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A4 Bitbucket Link](https://bitbucket.org/acc16/bitbucketstationlocations/ "Bitbucket Station Locations")


