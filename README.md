> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4331 Advanced Mobile App Development

## Ayanna Chukes

### LIS4331 Requirements:

*Course Work Links:*

1. [A1 README.md](A1/README.md "My A1 README.md file")
    - Install JDK
    - Install Android Studio and create My First App and My Contact App
    - Provide Screenshots of installations
    - Create Bitbucket repo 
    - Complete Bitbucket tutorials
    - Provide git command descriptions

2. [A2 README.md](A2/README.md "My A2 README.md file")
    - Create a Tip Calculator App
    - Chapter 3 Questions 
    - Bitbucket repo links: this assignment

3. [A3 README.md](A3/README.md "My A2 README.md file")
    - Create a Currency Converter App
    - Chapter 5 and 6 Questions 
    - Bitbucket repo links: this assignment

4. [P1 README.md](MusicPlayer/README.md "My P1 README.md file")
    - Create a Music Player App
    - Chapter 7 and 8 Questions 
    - Bitbucket repo links: this assignment

