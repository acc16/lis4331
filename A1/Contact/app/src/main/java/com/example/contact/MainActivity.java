package com.example.contact;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button bnContact1 =findViewById(R.id.bnContact1);
        Button bnContact2 =findViewById(R.id.bnContact2);
        Button bnContact3 =findViewById(R.id.bnContact3);

        final Intent intentContact1 = new Intent(this, ContactInfo1.class);
        final Intent intentContact2 = new Intent(this, ContactInfo2.class);
        final Intent intentContact3 = new Intent(this, ContactInfo3.class);

        bnContact1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(intentContact1);
            }
        });

        bnContact2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(intentContact2);
            }
        });

        bnContact3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(intentContact3);
            }
        });
    }
}
