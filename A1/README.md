> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331

## Ayanna Chukes

### Assignment # Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chp:1,2)
4. Bitbucket repo links

#### README.md file should include the following items:

* Screenshot of AMPPS Installation (My PHP Installation)(https://localhost:8080 "PHP Localhost);
* Screenshot of running java Hello; 
* Screenshot of running  Android Studio - My First App
* Git commands w/ short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create a new local repository
2. git status - List the files changed and the ones that still need to be added or commited.
3. git add - Add one or more files to staging 
4. git commit - commit changes or files that have been added
5. git push - send changes to master branch of remote repository
6. git pull - fetch and merge changes on the remote server to the working directory
7. git branch - list all the branches in the repo and tell current branch 

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)

*Screenshot of Android Studio - Contact App*:

![Android Studio Contact1 Screenshot](img/contact1.png)

![Android Studio Contact2 Screenshot](img/contact2.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/acc16/bitbucketstationlocations/ "Bitbucket Station Locations")

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket lis4331 Link](https://bitbucket.org/acc16/lis4331/ "Bitbucket Station Locations")


#### Questions:
1.
Which of the following is an open-source business partnership of 80 firms that develop standards for mobile devices?

c. Open Handset Alliance

2.
After the Android emulator is loaded, what must you do before you can run your app in 
it?

b. Unlock the virtual device


3.
By default, what does @string specified in a TextView control refer to?

a. A file called strings.xml

4.
If you want to modify the value of the de
fault TextView control in a new Android app, 
what should you do?

c. Edit the strings.xml file

5.
What happens when you click the Run 'app' button in Android Studio?

b. The Choose Device dialog box is displayed

6.
What should a * beginner* typically use to change the contents of the strings.xml file?

d. Translations Editor

7.
What string is placed in the default TextView control in a new Android app created using 
Android Studio?

b. "Hello World!"

8.
Android code was released under the Apache license which means the platform is 
considered which of the following?

c. Open source

9.
In Java, what is a tap of the touch screen known as?

a. click event

10.
In which folder are resources for medium
-
density screens placed?

c. drawable
-
mdpi folder

11.
The emulator mimics almost every feature of a real A
ndroid except for the ability to do 
what?

b. place a phone call

12.
What does SDK stands for?

b. Software Development Kit

13.
What does the unit sp stand for?

b. scaled-independent pixels

14.
What file lists the permissions needed to access other Android functions required by your 
app?

b. AndroidManifest.xml

15.
What folder keeps resources such as images and music?

c. res

16.
What is a part of a program coded to respond to a specific event?

b. event handler

17.
What is a * set* (or, group) of Java statements that can be included inside a Java class?

d. method

18.
What is the entry point of an Activity?

c. onCreate method

19.
What is the fee you must pay to Google for each free app you publish on Google Play?

c. No fee

20.
What is the first phase of program development?

c. gather program 
requirements

21.
What is the human
-
readable title for your application?
a. Application name

22.
What is the key to successful usage of an app ?

b. intuitive interface

23.
What is the return type of a method that does *not* return a value?

d. void

24.
What symbol indicates errors in program code?

d. red curly lines

25.
What term is used when creating an object from a class?

b. instantiate

26.
When centering a control, what kind of line is displayed when you drag the control to the center of the emulator window?

b. a green dashed vertical line

27.
When you create a new Android project, what is the default object placed into the 
emulator window?

c. TextView widget

28.
Where are apps written for Android sold and deployed?

c. Google Play

29.
Where do you need to put a picture file that will be used with an ImageView control?

b. resources folder

30.
Which control can display an icon or a graphic such as a picture file or shape?

c. ImageView control

31.
Which of the following abbreviations is not a
valid measurement unit for text display?

b. ft

32.
Which of the following is a container that can hold as many widgets as needed?

b. layout

33.
Which of the following wait for user interaction.

b. Event listeners

34.
Which phrase refers to how a user feels when using a particular device?

d. user experience

35.
Which programming language is used to 
write Android apps?

c. Java

36.
Which term best describes "a program that duplicates the look and feel of a particular 
device"?

b. emulator

37.
Which term best describes a bluep
rint or a template for creating objects by defining its 
properties?

b. class

38.
Which term defines the namespace where your code resides in Java?

c. Package name

39.
Which term describes a single element such as a TextView, Button, or CheckBox control?

d. widget

40.
Which type of layout organizes layout components in relation to each other?

d. Re
lative layout

41.
What are the components of a program that perform the tasks required in the program?

c. Processing objects

42.
What is contained in the Minimum SDK text box?

a. API number

43.
Which method creates an intent to start another Activity?

d. startActivity method

44.
Which method displays the content of a specific screen?

d. setContentView method

45.
Which of the following is NOT a feature supported by the Android platform?
(
Note: now 
allows multiple monitors.)

b. Multiple monitors

46.
Which of the following statements is a way of making more Java functions available to 
your specific program?

b. import statement

47.
Which of the following terms best desc
ribes a piece of code that actually serves as a 
placeholder to declare itself?

d. stub

48.
Which part of Android Studio contains the key source application folders for the project?

c. Android project view

49.
Which type of layout organizes layout components in a vertical column or horizontal 
row?

c. Linear layout
