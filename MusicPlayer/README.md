# LIS4331

## Ayanna Chukes

### Project 1 # Requirements:

1. Include splash screen image, app title, intro text.
2. Include artists’ images and media.
3. Images and buttons must be vertically and horizontally aligned.
4. Must add background color(s) or theme
5. Create and displaylauncher icon image

#### README.md file should include the following items:

1. Course title, your name, assignment requirements, as per A1;
2. Screenshot of running application’s splash screen;
3. Screenshot of running application’s follow-up screen (with images and buttons);
4. Screenshots of running application’s play and pause user interfaces (with images and buttons);

Deliverables:
1. Provide Bitbucket read-only access to lis4331repo, include links to the repos you created in the above tutorials in README.md, using Markdown syntax(README.md must also include screenshots as per above.)
2. Blackboard Links: lis4331 Bitbucket

#### Assignment Screenshots:

*Screenshot of splash screen user interface running*:

![Splash Screen Screenshot](img/splash.png)

*Screenshot of follow-up user interface running*:

![Follow-up Screen Screenshot](img/followup.png)

*Screenshot of play user interface running*:

![Play Screenshot](img/play.png)

*Screenshot of pause user interface running*:

![Pause Screenshot](img/pause.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/acc16/bitbucketstationlocations/ "Bitbucket Station Locations")

#### Questions:

1. In the Calendar class, which
method changes the date or time?
c. set

2. To create a new column to a TableLayout, what do you do?
a. add a view to a row

3. To create an instance of the Calendar class, what do you call?
a. Calendar.getInstance()

4. To set the style of text to a particular font family, which property can you use?
d. typeface property

5. Using controls such as the GridView and Spinner, what binds specific types of data and 
displays that data in a particular layout?
a. adapter

6. What are the three navigation buttons centered on the bottom of the
Android tablet screen?
c. Back, Home, Multitasking

7. What category of the Palette does the control GridView sit in?
b. Composite category

8. What field manipulation method accesses the system date or time?
a. get

9. What is AVD short for?
c. Android Virtual Devices

10. What is the first GridView control in a project named by default?
c. gridView1

11. What is the return type of a method that returns nothing?
d. void

12. What is the widget to
select dates called?
c. DatePicker widget

13. What is the width of a table column determined by?
c. width of the largest view

14. What is typically used to assign values to a GridView control that has multiple items?
a. array

15. What is used to identify each element in an array?
b. index

16. What is used to initialize the instance variables of an object?
c. constructor

17. What should you use to declare a variable that cannot be changed?
b. final

18. What type of control is usually used to select a 
wallpaper image for the Android device 
background?
b. ImageViewer

19. When the ImageAdapter class is called, which method determines how many pictures 
should be displayed in the GridView control?
b. getCount() method

20. When you create an Android tablet app, what should the minimum required SDK should be 
set to in order to cover the oldest Android tablets?
d. API 13: Android 3.2 (Honeycomb)

21. Where do you specify a method’s return type?
b. in its declaration

22. Which Android operating syst
em is designed specifically for tablet use?
c. Honeycomb

23. Which argument of the onItemClick method determines the row of the selected item?
a. id

24. Which argument of the onI
temClick method holds the value of the array element in the 
View adapter?
c. position

25. Which attribute in XML code can be used to place an image on the surface of a Button 
control?
d. android:src

26. Which class is responsible for converting between a Date object and a set of integer fields 
such as YEAR, MONTH, and so on.
a. Calendar class

27. Which event is fired after the use
r sets a date selection?
b. onDateSetListener

28. Which is a common aspect ratio?
a. 4:3

29. Which layout can you use to create a layout that contains a simple arrangement of r
ows 
and columns?
d. TableLayout

30. Which layout creates a simple, clean interface containing rows and columns?
d. LinearLayout

31. Which method can you use to display the device's system year, month, and day of the 
month in the Datepicker?
d. show()

32. Which method provides a number of arguments to its registered listeners?
d. onItemClick method

33. Which of the following applications creates an optimal user experience based on common 
tablet screen size?
b. native application

34. Which of the following does the Calendar class NOT use to access different fields?
b. WEEK_OF_MONTH

35. Which of the following is NOT an attribute of a GridView control?
a. leftMargin

36. Which of the following is not a calendar control?
d. ScheduledClock

37. Which of the following is not a recommended design approach?
a. Fill the large screen

38. Which of the following is the scaling option that scales an image to fit the View type?
d. ImageView.ScaleType.FIT_XY

39. Which of the following is true about toast notifications?
a. They never receive focus

40. Which of the following statements sets the desired year in the onDateSet method?
c. c.set(Calendar.YEAR, year); 
 
41. 
Which of the following terms best describes the fractional relation of the width of an image 
compared with its height?
a. aspect ratio

42. Which of the following terms defines a rectangular area of the scr
een that displays an 
image or text object.
d. View container

43. Which of the following variable types can be accessed from multiple methods throughout an 
application?
b. Class-level

44. Which of the following variables is used to load and access resources for the application?
b. Context variable

45. Which property can be used to offset the content of a 
control by a specific number of 
pixels?
b. padding property

46. Which scaling option does not scale or change the aspect ratio of an image?
a. ImageView.ScaleType.CENTER

47. Which statement creates an instance of the Calendar class?
d. Calendar c = Calendar.getInstance();

48. Which statement do you place at the end of a method if the method sends a value to the 
statement that called it?
c. return

49. Which type of application is converted in real time to
run on a variety of platforms?
a. emulated application

50. Which type of application is converted in real time to run on a variety of platforms?
b. Emulated
