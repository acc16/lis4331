package com.example.musicplayer;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {


    Button btn1, btn2, btn3;
    MediaPlayer mpAdele, mpSki, mpJuice;
    int playing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        btn1 = findViewById(R.id.btnAdele);
        btn2 = findViewById(R.id.btnSki);
        btn3= findViewById(R.id.btnJuice);
        btn1.setOnClickListener(bAdele);
        btn2.setOnClickListener(bSki);
        btn3.setOnClickListener(bJuice);
        mpAdele = new MediaPlayer();
        mpAdele = MediaPlayer.create(this, R.raw.adele);
        mpSki = new MediaPlayer();
        mpSki = MediaPlayer.create(this, R.raw.ski);
        mpJuice = new MediaPlayer();
        mpJuice = MediaPlayer.create(this, R.raw.juicewrld);
        playing = 0;
    }

    Button.OnClickListener bAdele= new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch(playing){
                case 0:
                mpAdele.start();
                playing = 1;
                btn1.setText("Pause adele");
                btn2.setVisibility(View.INVISIBLE);
                btn3.setVisibility(View.INVISIBLE);
                break;
                case 1:
                    mpAdele.pause();
                    playing = 0;
                    btn1.setText("Play adele");
                    btn2.setVisibility(View.VISIBLE);
                    btn3.setVisibility(View.INVISIBLE);
                    break;

            }

        }
    };
    Button.OnClickListener bSki = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
                    switch(playing){
                        case 0:
                            mpSki.start();
                            playing = 1;
                            btn2.setText("Pause Ski Mask");
                            btn1.setVisibility(View.INVISIBLE);
                            btn3.setVisibility(View.INVISIBLE);
                            break;
                        case 1:
                            mpSki.pause();
                            playing = 0;
                            btn2.setText("Play Ski Mask");
                            btn1.setVisibility(View.VISIBLE);
                            btn3.setVisibility(View.VISIBLE);
                            break;

                    }

                }


    };
    Button.OnClickListener bJuice = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch(playing){
                case 0:
                    mpJuice.start();
                    playing = 1;
                    btn3.setText("Pause Juice Wrld");
                    btn2.setVisibility(View.INVISIBLE);
                    btn1.setVisibility(View.INVISIBLE);
                    break;
                case 1:
                    mpSki.pause();
                    playing = 0;
                    btn3.setText("Play Juice Wrld");
                    btn2.setVisibility(View.VISIBLE);
                    btn1.setVisibility(View.VISIBLE);
                    break;

            }

        }


    };

}
